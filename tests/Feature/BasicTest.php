<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BasicTest extends TestCase
{
    /**
     * A basic feature landing page.
     *
     * @return void
     */
    public function test_landing_page()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * A basic feature history page.
     *
     * @return void
     */
    public function test_history_page()
    {
        $response = $this->get('/history');

        $response->assertStatus(200);
    }

    /**
     * A basic feature landing page.
     *
     * @return void
     */
    public function test_changing_price_request()
    {

        $response = $this->withHeaders([
            '_token' => csrf_token(),
        ])->from('/')->postJson('/change_price', ['price' => 1]);

        $response->assertRedirect('/');
        $this->assertTrue($response->isRedirection());
    }

    /**
     * A basic feature landing page.
     *
     * @return void
     */
    public function test_purchase_request()
    {
        $response = $this->withHeaders([
            '_token' => csrf_token(),
        ])->from('/')->post('/purchase', ['quantity' => 1, 'price' => 1]);

        $response->assertRedirect('/');
        $this->assertTrue($response->isRedirection());
    }

    /**
     * A basic feature landing page.
     *
     * @return void
     */
    public function test_apply_unit_request()
    {
        $response = $this->withHeaders([
            '_token' => csrf_token(),
        ])->from('/')->post('/apply_unit', ['quantity' => 1]);

        $response->assertRedirect('/');
        $this->assertTrue($response->isRedirection());
    }
}
