@extends('layout.base')

@section('content')
    <form action="{{ route('save.purchase') }}" method="post">
        @csrf
        <div class="row g-3 align-items-center">
            <div class="col-auto mb-4">
                <label class="col-form-label">Product Purchase: </label>
            </div>
            <div class="col-auto">
                <input type="number" step="1" class="form-control" placeholder="0" min="0" name="quantity" /><small>Product Price is: <span id="product_price">${{ number_format( $product ? $product->price : 0, 2) }}</span></small>
            </div>
            <div class="col-auto mb-4">
                <input type="submit" class="btn btn-primary" value="Submit" />
                <input type="button" id="change_price" class="btn btn-info" value="Change Price" />
            </div>
        </div>
    </form>
    <form action="{{ route('save.applied_unit') }}" method="post" class="mb-5">
        @csrf
        <div class="row g-3 align-items-center mt-3">
            <div class="col-auto">
                <label class="col-form-label">Applied Units: </label>
            </div>
            <div class="col-auto">
                <input type="number" step="1" class="form-control" placeholder="0" min="0" name="quantity" />
            </div>
            <div class="col-auto">
                <input type="submit" class="btn btn-success" value="Apply"/>
            </div>
        </div>
    </form>

    <label> Quantity on hand: @if(count($items_on_hand) > 0) {{ $total }} Valuation = @foreach($items_on_hand as $key => $item)({{ $item['quantity'] }} * {{ $item['price'] }}) {{ $key + 1 == count($items_on_hand) ? '' : '+' }} @endforeach = ${{ $total_price }} @else 0 Unit @endif</label>

    <div id="change_price_modal" class="modal fade" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document" style=" width:auto; max-width:500px;">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Change Price</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('change.price') }}" method="post">
                    @csrf
                    <div class="modal-body pd-25">
                        <div class="col-auto">
                            <label class="col-form-label">Product Price: </label>
                        </div>
                        <div class="col-auto">
                            <input type="number" step="0.01" class="form-control" value="{{ $product ? $product->price : 0 }}" min="0" name="price"/>Current Price is: <span id="product_price">${{ number_format( $product ? $product->price : 0, 2) }}</span>
                        </div>
                    </div>
                    <div class="modal-footer mt-5" style="justify-content:center;">
                        <button type="button" class="btn btn-outline-warning close-modal" data-dismiss="modal" style="display: inline-block; width: 150px;">Close</button>
                        <button type="submit" class="btn btn-primary" style="display: inline-block; width: 150px;">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- modal -->

    <table id="history_list">
        <thead>
            <tr>
                <th>Date</th>
                <th>Type</th>
                <th>Quantity</th>
                <th>Unit Price</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
@endsection

@section('script')
    <script src="{{ config(' app.cdn ') . '/js/product/computation.js' }}"></script>
    <script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment.min.js"></script>
@endsection
