$(function () {
    'use strict'

    // setup of datatables
    $('#history_list').DataTable({
        dom: '<"float-left"f><"float-right"B>rt<"row"<"col-sm-4"i><"col-sm-8"<"row"<"col-sm-10"p><"col-sm-2 float-right" l>>>>',
        sorting: [],
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '&nbsp; _MENU_',
        },
        ajax: {
            type: 'GET',
            url: `history`
        },
        columns: [
            {data:"created_at", name:"date", render: function(data, type) {
                // we use render here because datatable sorting rendered data
                return type === 'sort' ? data : moment(data).format('LL')
            }},
            {data:'type', name:'type'},
            {data:'quantity', name:'quantity'},
            {data:"price", name:'unit_price', render: function(data, type) {
                // we use render here because we want to add dollar sign and a - sign when there is 0 value
                return data ? '$' + data : '-'
            }},
            {data:"total_price", name:'total_price', render: function(data, type) {
                // we use render here because we want to add dollar sign and a - sign when there is 0 value
                return data ? '$' + data : '-'
            }}
        ]
    })

    $('#change_price').click(function(){
        // display the modal when clicked
        $('#change_price_modal').modal('show')
    })
})
