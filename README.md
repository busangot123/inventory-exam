# DSS Coding Exam
## Setting up
- Rename .env.example to .env
- Setup .env variable and connect to local database
    change the following on the .env file
    DB_DATABASE=
    DB_USERNAME=
    DB_PASSWORD=
- inside root directory run `php artisan migrate`
- inside root directory run `php artisan db:seed`
- inside root directory `npm run dev`

## Note

- if no xampp or other virtual box, run `php -S localhost:8000 -t public` to enable the port 8000 on the browser
- in the browser there is a button to change the price of the product i used it to check the data in different prices
- there is a table at the bottom for the list of record it is sorted in descending order so latest record appear on the top
- error and success notes will popup at the upper right of the screen

## Plugins used
- toastr https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.0/js/toastr.js
- bootstrap https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js
- jquery https://code.jquery.com/jquery-3.5.1.js
- moment https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment.min.js
- datatables https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js
