<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\PurchaseHistory;

class UserController extends Controller
{
    /**
    *
    * function to setup all needed variable for frontend use
    *
    */
    public function index()
    {
        // get latest product record
        $product = Product::latest()->first();

        // get count of application and purchased record
        $application_count = PurchaseHistory::applicationTotal();
        $purchase_count = PurchaseHistory::purchaseTotal();
        $difference = $purchase_count - $application_count;

        // get all transaction that has a purchase type
        $purchase_list = PurchaseHistory::whereType('Purchase')->orderByDesc('created_at')->get();

        // new collection setup for counting of quantities of product unit
        $items_on_hand = [];

        // total records for display
        $total = 0;
        $total_price = 0;

        //negative flag so that negative values will not listed in the items_on_hand variable
        $negative_counter = 0;

        foreach($purchase_list as $item) {
            // subtracting the quantity to the difference so that we can get how many left to the quantity
            $difference -= $item->quantity;
            if($difference >= 0) {
                // if it is still more than or equal to 0 item will be listed
                $items_on_hand[] = [
                    'quantity' => $item->quantity,
                    'price' => $item->price
                ];

                //calculation of totals
                $total += $item->quantity;
                $total_price += $item->quantity * $item->price;
            } else if ($difference < 0 && $negative_counter == 0) {
                // if there is less than the value we will get the difference in absolute value so that we can know how many left on that purchase unit

                // negative counter will be triggered to avoid duplication of negative difference
                $negative_counter++;

                // item will be listed on how many left to their quantity
                $items_on_hand[] = [
                    'quantity' => abs($difference),
                    'price' => $item->price
                ];

                //calculation of totals
                $total += abs($difference);
                $total_price += abs($difference) * $item->price;
            }
        }

        return view('welcome', compact('product', 'items_on_hand', 'total', 'total_price'));
    }
}
