<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\PurchaseHistory;
use Yajra\DataTables\DataTables;

class ProductController extends Controller
{
    /**
    *
    * function to update price of the product
    * default price will be zero
    */
    public function changePrice(Request $request)
    {
        // get list of all the product
        $product_list = Product::all();

        // check if there is an existing record inside the database
        if(count($product_list) > 0) {
            $product = $product_list->first();
            $product->price = $request->price;
            $product->update();
        } else {
            // create new product if not yet existing
            $product = new Product();
            $product->create([
                'price' => $request->price
            ]);
        }

        return redirect()->back();
    }

    /**
    *
    * function to create record of purchase unit
    *
    */
    public function purchase(Request $request)
    {
        // get the latest record of product in the database
        $product = Product::latest()->first();

        // check if there is an existing pricing for the product
        if(!$product) {
            return redirect()->back()->with('error', 'Failed! Set price for the product');
        }

        // create new record of purchase history
        $history = new PurchaseHistory();
        $history->create([
            'user_id' => User::latest()->first()->id,
            'type' => 'Purchase',
            'quantity' => $request->quantity,
            'price' => $product->price,
            'total_price' => $product->price * $request->quantity
        ]);

        return redirect()->back()->with('success', 'Success! Purchased Unit');
    }

    /**
    *
    * function to create record of applied unit
    *
    */
    public function applyUnit(Request $request)
    {
        // get the count of all quantity of product that already applied
        $application_count = PurchaseHistory::applicationTotal();

        //get all quantity of all product purchased
        $purchase_count = PurchaseHistory::purchaseTotal();

        // check the difference for validation
        $difference = $purchase_count - $application_count - $request->quantity;

        // return an error response if there is no enough product unit to apply
        if($difference < 0){
            return redirect()->back()->with('error', 'Failed! Not Enough Purchased Unit');
        }

        $history = new PurchaseHistory();
        $history->create([
            'user_id' => User::latest()->first()->id,
            'type' => 'Application',
            'quantity' => '-' . $request->quantity,
            'price' => 0,
            'total_price' => 0
        ]);

        return redirect()->back()->with('success', 'Success! Applied Unit');
    }

    /**
    *
    * endpoint made for ajax request of the table
    *
    */
    public function history()
    {
        // get list of all transaction made
        $history = PurchaseHistory::orderByDesc('created_at')->get();
        return DataTables::of($history)->make(true);
    }
}
