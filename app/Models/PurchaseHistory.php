<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseHistory extends Model
{
    use HasFactory;

    protected $table = 'purchase_histories';
    protected $fillable = [
        'user_id',
        'quantity',
        'type',
        'price',
        'total_price'
    ];

    /**
    *    get summation of all already applied units
    */
    public static function applicationTotal()
    {
        $application_list = Self::whereType('Application')->sum('quantity');
        return abs($application_list);
    }

    /**
    *    get summation of all already purchased units
    */
    public static function purchaseTotal()
    {
        $application_list = Self::whereType('Purchase')->sum('quantity');
        return abs($application_list);
    }
}
