<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index')->name('user.index');
Route::post('change_price', 'ProductController@changePrice')->name('change.price');
Route::post('purchase', 'ProductController@purchase')->name('save.purchase');
Route::post('apply_unit', 'ProductController@applyUnit')->name('save.applied_unit');
Route::get('history', 'ProductController@history')->name('history');
